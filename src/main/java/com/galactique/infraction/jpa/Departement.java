package com.galactique.infraction.jpa;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Departement implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String libelle;
    private String region;

    /*@JsonIgnore
    @ManyToOne
    @JoinColumn(name="region_id", nullable=false)
    private Region region;

    /*@OneToMany(mappedBy = "departement")
    private List<Arrondissement> arrondissements;*/

}
