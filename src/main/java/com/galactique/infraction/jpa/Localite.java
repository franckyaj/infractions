package com.galactique.infraction.jpa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Audited
public class Localite implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String region;
    private String departement;
    private String arrondissement;
    private String unite;
    private String delegation;

    @JsonIgnore
    @OneToOne(mappedBy = "localite")
    private Infraction infraction;

    /*@JsonIgnore
    @OneToMany(mappedBy = "region")
    private List<Departement> departements;*/
}
