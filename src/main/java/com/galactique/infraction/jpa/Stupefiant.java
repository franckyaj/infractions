package com.galactique.infraction.jpa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.galactique.infraction.base.ValidForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Audited
public class Stupefiant extends ValidForm {

    @Id
    @GeneratedValue
    private Long id;
    private String nature;
    private int String;
    private String provenance;
    private String destinattion;
    private String etat;
    private String quantite;
    @Column(columnDefinition = "boolean default true")
    private boolean active = true;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    /*@JsonIgnore
    @ManyToOne
    @JoinColumn(name="enquete_id", nullable = true)
    private Enquete enquetes;*/

}
