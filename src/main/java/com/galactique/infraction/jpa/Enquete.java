package com.galactique.infraction.jpa;


import com.galactique.infraction.base.ValidForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Audited
public class Enquete extends ValidForm implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String referenceProcedure;
    private String parquetCompetant;
    @Column(name= "affaire", columnDefinition="TEXT",length=2147483647)
    private String affaire;
    private String cadreJuridique;
    private String coordonneesOpj;
    private String coordonneesAssistant;
    @Column(name= "resumeFaits", columnDefinition="TEXT",length=2147483647)
    private String resumeFaits;
    @Column(columnDefinition = "boolean default true")
    private boolean active = true;

    @ManyToMany
    @JoinTable(
            name = "enquetes_victimes",
            joinColumns=@JoinColumn(name="enqueteid"),
            inverseJoinColumns=@JoinColumn(name="victimeid"))
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Victime> victimes = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "enquetes_vehiculevole",
            joinColumns=@JoinColumn(name="enqueteid"),
            inverseJoinColumns=@JoinColumn(name="vehiculeid"))
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<VehiculeVole> vehiculeVoles = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "enquetes_mec",
            joinColumns=@JoinColumn(name="enqueteid"),
            inverseJoinColumns=@JoinColumn(name="mecid"))
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<MiseEnCause> miseencauses = new ArrayList<>();

    @OneToMany
    @JoinColumn(name = "stupefiant_id")
    private List<Stupefiant> stupefiants = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "enquetes_armes",
            joinColumns=@JoinColumn(name="enqueteid"),
            inverseJoinColumns=@JoinColumn(name="armeid"))
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Arme> armes = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "infractionid", referencedColumnName = "id")
    private Infraction infraction;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;


}
