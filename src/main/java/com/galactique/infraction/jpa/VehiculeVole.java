package com.galactique.infraction.jpa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Audited
public class VehiculeVole implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String marque;
    private String immatriculation;
    private String chasis;
    private String etat;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    private String proprietaire;
    @Column(columnDefinition = "boolean default true")
    private boolean active = true;

    @JsonIgnore
    @ManyToMany(mappedBy="vehiculeVoles")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Enquete> enquetes = new ArrayList<>();


}
