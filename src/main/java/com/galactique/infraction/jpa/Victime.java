package com.galactique.infraction.jpa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.galactique.infraction.base.GenericPerson;
import com.galactique.infraction.enumeration.TypeSexe;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Audited
public class Victime extends GenericPerson {

    @JsonIgnore
    @ManyToMany(mappedBy="victimes")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Enquete> enquetes = new ArrayList<>();

}
