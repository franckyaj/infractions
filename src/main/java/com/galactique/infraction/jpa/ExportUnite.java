package com.galactique.infraction.jpa;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "export_unite")
public class ExportUnite implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String delegation;
    private String unite;
}
