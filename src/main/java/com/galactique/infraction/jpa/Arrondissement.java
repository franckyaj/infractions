package com.galactique.infraction.jpa;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Arrondissement implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String libelle;
    private String departement;

    /*@JsonIgnore
    @ManyToOne
    @JoinColumn(name="dept_id", nullable=false)
    private Departement departement;*/
}
