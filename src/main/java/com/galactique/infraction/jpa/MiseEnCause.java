package com.galactique.infraction.jpa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.galactique.infraction.base.GenericPerson;
import com.galactique.infraction.base.ValidForm;
import com.galactique.infraction.enumeration.TypeSexe;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.envers.Audited;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Audited
public class MiseEnCause extends GenericPerson {

    private String alias;
    private String gav;
    private String implication;
    private String situation;
    private String etat;

    @JsonIgnore
    @ManyToMany(mappedBy="miseencauses")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Enquete> enquetes = new ArrayList<>();


    @ElementCollection
    private List<String> infractionCommises;

}
