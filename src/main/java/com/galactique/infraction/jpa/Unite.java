package com.galactique.infraction.jpa;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class Unite implements Serializable {

    @Id
    @GeneratedValue
    private Long isd;
    private String region;
    private String unite;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
}
