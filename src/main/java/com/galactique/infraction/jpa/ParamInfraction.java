package com.galactique.infraction.jpa;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class ParamInfraction implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String categorieInfraction;
    private String designationInfraction;
    private String posteInfraction;

    @Column(columnDefinition = "boolean default true")
    private boolean active;


}
