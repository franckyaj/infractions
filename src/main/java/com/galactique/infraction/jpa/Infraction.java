package com.galactique.infraction.jpa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.galactique.infraction.base.ValidForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Audited
public class Infraction extends ValidForm implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String categorie;
    @ElementCollection
    private List<String> infractionCommises;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    private String typeSaisie;
    private String adressePrecise;
    @Column(columnDefinition = "boolean default true")
    private boolean active = true;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "localiteid", referencedColumnName = "id")
    private Localite localite;

    private String rreferenceSaisie;

    @JsonIgnore
    @OneToOne(mappedBy = "infraction")
    private Enquete enquete;

}
