package com.galactique.infraction.jpa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.galactique.infraction.base.ValidForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Audited
public class Arme extends ValidForm implements Serializable{

    @Id
    @GeneratedValue
    private Long id;
    private String type;
    private String numero;
    private String etat;
    @Column(columnDefinition = "boolean default true")
    private boolean active = true;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @JsonIgnore
    @ManyToMany(mappedBy="armes")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Enquete> enquetes;


}
