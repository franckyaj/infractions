package com.galactique.infraction.service;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.Infraction;

import java.util.List;

public interface IInfraction {

    public Infraction add(Infraction infraction);
    public boolean delete(Long id);
    public List<Infraction> filter(FilterRequestDto dto);
}
