package com.galactique.infraction.service;

import com.galactique.infraction.dto.LocaliteDto;
import com.galactique.infraction.dto.RegionDto;
import com.galactique.infraction.dto.UniteDto;
import com.galactique.infraction.dto.UniteListDto;
import com.galactique.infraction.jpa.Localite;
import com.galactique.infraction.jpa.Unite;

import java.util.List;

public interface ILocalite {

    public List<Localite> filter(Localite localite);
    public List<Localite> findAll();
    public List<Localite> add(LocaliteDto dto);
    public Localite save(Localite entity);
    public boolean delete(Long id);
    public List<String> getDepartement(String region);
    public List<String> getArrondissement(String dept);
    public List<Unite> saveListUnite(UniteListDto uniteListDto);
    public List<Unite> filterUnite(UniteDto dto);
    public List<String> getRegions();

}
