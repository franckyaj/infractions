package com.galactique.infraction.service;

import com.galactique.infraction.dto.ToolRespDto;

public interface ITool {

    public ToolRespDto filter();
}
