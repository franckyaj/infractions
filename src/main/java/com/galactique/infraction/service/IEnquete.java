package com.galactique.infraction.service;

import com.galactique.infraction.dto.EnqueteUpdateReqDto;
import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.Enquete;

import java.util.List;

public interface IEnquete {

    public Enquete add(Enquete enquete);
    public Enquete edit(EnqueteUpdateReqDto dto);
    public boolean delete(Long id);
    public List<Enquete> filter(FilterRequestDto dto);
}
