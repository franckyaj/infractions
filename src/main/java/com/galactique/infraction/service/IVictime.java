package com.galactique.infraction.service;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.Victime;

import java.util.List;

public interface IVictime {

    public Victime add(Victime entity);
    public List<Victime> addList(List<Victime> victimes);
    public List<Victime> filter(FilterRequestDto dto);
    public boolean delete(Long id);
}
