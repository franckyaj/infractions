package com.galactique.infraction.service;

import com.galactique.infraction.jpa.ExportUnite;
import com.galactique.infraction.jpa.ExportUniteDto;

import java.util.List;

public interface IExportUnite {

    public List<ExportUnite> saveAll(ExportUniteDto dto);
    public List<ExportUnite> getAll();
    public List<String> getDeletation(String region);
    public List<String> getUniteByDelegation(String delegation);
}
