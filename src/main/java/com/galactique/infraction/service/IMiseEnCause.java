package com.galactique.infraction.service;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.MiseEnCause;

import java.util.List;

public interface IMiseEnCause {

    public MiseEnCause add(MiseEnCause entity);
    public List<MiseEnCause> addList(List<MiseEnCause> miseEnCauses);
    public List<MiseEnCause> filter(FilterRequestDto dto);
    public boolean delete(Long id);

}
