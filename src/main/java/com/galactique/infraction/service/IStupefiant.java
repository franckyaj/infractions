package com.galactique.infraction.service;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.Stupefiant;

import java.util.List;

public interface IStupefiant {

    public Stupefiant add(Stupefiant stupefiant);
    public List<Stupefiant> addList(List<Stupefiant> stupefiants);
    public List<Stupefiant> filter(FilterRequestDto dto);
    public boolean delete(Long id);
}
