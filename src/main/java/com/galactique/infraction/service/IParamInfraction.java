package com.galactique.infraction.service;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.ParamInfraction;

import java.util.List;

public interface IParamInfraction {

    public List<ParamInfraction> filter(FilterRequestDto dto);
    public List<ParamInfraction> saveList(List<ParamInfraction> liste);
}
