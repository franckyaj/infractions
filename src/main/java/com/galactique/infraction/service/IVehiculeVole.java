package com.galactique.infraction.service;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.VehiculeVole;

import java.util.List;

public interface IVehiculeVole {

    public VehiculeVole add(VehiculeVole vehiculeVole);
    public List<VehiculeVole> addList(List<VehiculeVole> vehiculeVoles);
    public List<VehiculeVole> filter(FilterRequestDto dto);
    public boolean delete(Long id);
}
