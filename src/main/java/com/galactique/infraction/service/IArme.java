package com.galactique.infraction.service;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.Arme;

import java.util.List;

public interface IArme {

    public Arme add(Arme arme);
    public List<Arme> addList(List<Arme> armes);
    public List<Arme> filter(FilterRequestDto dto);
}
