package com.galactique.infraction.repository;

import com.galactique.infraction.jpa.Enquete;
import com.galactique.infraction.jpa.Infraction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

public interface InfractionRepository extends JpaRepository<Infraction, Long> {
}
