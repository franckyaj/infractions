package com.galactique.infraction.repository;

import com.galactique.infraction.jpa.ParamInfraction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ParamInfractionRepository extends JpaRepository<ParamInfraction, Long> {

    @Query("select distinct l.categorieInfraction from ParamInfraction l  where l.active = true")
    public List<String> getCategories();

    @Query("select distinct l.categorieInfraction from ParamInfraction l where l.active = true and l.designationInfraction = ?1")
    public List<String> getCategorie(String designaitonInfraction);

    @Query("select distinct l.designationInfraction from ParamInfraction l where l.active = true and l.categorieInfraction = ?1")
    public List<String> getInfractionComise(String categorie);

    @Query("select distinct l.designationInfraction from ParamInfraction l where l.active = true")
    public List<String> getDistinctInfractionComise();

}
