package com.galactique.infraction.repository;

import com.galactique.infraction.jpa.Localite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LocaliteRepository extends JpaRepository<Localite, Long> {

    @Query("select distinct l.region from Localite l ")
    public List<String> getRegion();

    @Query("select distinct l.departement from Localite l where l.region = ?1")
    public List<String> getDepartement(String region);

    @Query("select distinct l.arrondissement from Localite l where l.departement = ?1")
    public List<String> getArrondissement(String departement);

    @Query("select distinct l from Localite l where l.region = ?1 and l.departement= ?2 and l.arrondissement=?3")
    public List<String> fintlocalite(String region, String departement, String arrondissement);
}
