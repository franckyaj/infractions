package com.galactique.infraction.repository;

import com.galactique.infraction.jpa.Unite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnniteRepository extends JpaRepository<Unite, Long> {
}
