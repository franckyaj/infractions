package com.galactique.infraction.repository;

import com.galactique.infraction.jpa.Stupefiant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StupefiantRepository extends JpaRepository<Stupefiant, Long> {
}
