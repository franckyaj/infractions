package com.galactique.infraction.repository;

import com.galactique.infraction.jpa.Victime;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VictimeRepository extends JpaRepository<Victime, Long> {
}
