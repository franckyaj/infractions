package com.galactique.infraction.repository;

import com.galactique.infraction.jpa.Enquete;
import com.galactique.infraction.jpa.MiseEnCause;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;

import java.util.List;

public interface MiseEnCauseRepository extends JpaRepository<MiseEnCause, Long> {

    @Query("select m from MiseEnCause m where m.nom=?1 and m.prenom=?2")
    List<MiseEnCause> getMecByNomPrenom(String nom, String prenom);
}
