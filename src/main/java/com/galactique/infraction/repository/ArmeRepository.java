package com.galactique.infraction.repository;

import com.galactique.infraction.jpa.Arme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

public interface ArmeRepository extends JpaRepository<Arme, Long> {
}
