package com.galactique.infraction.repository;

import com.galactique.infraction.jpa.ExportUnite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ExportUniteRepository extends JpaRepository<ExportUnite, Long> {

    @Query("select distinct l.delegation from ExportUnite l where l.delegation like %:region%")
    public List<String> getDelegation(@Param("region") String region);

    @Query("select distinct l.unite from ExportUnite l where l.delegation = ?1")
    public List<String> getUniteByDelegation(String delegation);
}
