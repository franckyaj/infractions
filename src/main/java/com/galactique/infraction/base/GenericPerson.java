package com.galactique.infraction.base;

import com.galactique.infraction.enumeration.TypeSexe;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@MappedSuperclass
@Audited
public class GenericPerson extends ValidForm implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String nom;
    private String prenom;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateNaissance;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    private String lieuNaissance;
    private TypeSexe sexe;
    private String nationalite;
    private String autreNationalite;
    private String etat;
    private String numeroCni;
    private String adresse;
    private String telephone;
    @Column(columnDefinition = "boolean default true")
    private boolean cameroonian;
    @Column(columnDefinition = "boolean default false")
    private boolean neevers;

    @Column(columnDefinition = "boolean default true")
    private boolean active = true;

}
