package com.galactique.infraction.serviceimpl;

import com.galactique.infraction.dto.ToolRespDto;
import com.galactique.infraction.jpa.*;
import com.galactique.infraction.repository.*;
import com.galactique.infraction.service.ITool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ToolImpl implements ITool {

    @Autowired
    VictimeRepository victimeRepository;

    @Autowired
    MiseEnCauseRepository miseEnCauseRepository;

    @Autowired
    ParamInfractionRepository paramInfractionRepository;

    @Autowired
    StupefiantRepository stupefiantRepository;

    @Autowired
    ArmeRepository armeRepository;

    @Autowired
    VehiculeVoleRepository vehiculeVoleRepository;

    @Autowired
    LocaliteRepository localiteRepository;

    @Override
    public ToolRespDto filter() {
        ToolRespDto dto = new ToolRespDto();

        List<String> marques = getVehiculeMarque();
        List<String> natures = getStupefiantNature();
        List<String> names = getPersonneNAmes();
        List<String> types = getArmeType();
        List<String> regions = getRegions();
        List<String> categories = getCategorieInfractions();

        dto.getMarques().addAll(marques);
        dto.getTypes().addAll(types);
        dto.getNatures().addAll(natures);
        dto.getNoms().addAll(names);
        dto.getRegions().addAll(regions);
        dto.getCategories().addAll(categories);


        return dto;
    }

    public List<String> getPersonneNAmes() {

        List<String> names = new ArrayList<>();
        List<Victime> victimes = victimeRepository.findAll();
        List<MiseEnCause> miseEnCauses = miseEnCauseRepository.findAll();
        List<String> victimeNames = new ArrayList<>();
        List<String> mecNames = new ArrayList<>();

        for(Victime v: victimes) victimeNames.add(v.getNom());
        for(MiseEnCause m: miseEnCauses) mecNames.add(m.getNom());

        names.addAll(victimeNames);
        names.addAll(mecNames);

        Set<String> results = new HashSet<>(names);
        names.clear();
        names.addAll(results);


        return names;

    }

    public List<String> getStupefiantNature() {

        List<String> natures = new ArrayList<>();
        List<Stupefiant> stupefiants = stupefiantRepository.findAll();
        for(Stupefiant s: stupefiants) natures.add(s.getNature());

        return natures;

    }


    public List<String> getArmeType() {

        List<String> types = new ArrayList<>();
        List<Arme> armes = armeRepository.findAll();
        for(Arme s: armes) types.add(s.getType());

        return types;
    }

    public List<String> getCategorieInfractions() {

        List<String> paramInfractions = paramInfractionRepository.getDistinctInfractionComise();

        return paramInfractions;
    }

    public List<String> getVehiculeMarque() {

        List<String> marques = new ArrayList<>();
        List<VehiculeVole> vehiculeVoles = vehiculeVoleRepository.findAll();
        for(VehiculeVole s: vehiculeVoles) marques.add(s.getMarque());

        return marques;
    }

    public List<String> getRegions() {
        List<String> res = new ArrayList<>();
        List<String> regions = localiteRepository.getRegion();
        return regions;
    }
}
