package com.galactique.infraction.serviceimpl;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.Arme;
import com.galactique.infraction.jpa.Victime;
import com.galactique.infraction.repository.ArmeRepository;
import com.galactique.infraction.service.IArme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;


@Service
public class ArmeImpl implements IArme {

    @Autowired
    ArmeRepository armeRepository;

    @Autowired
    EntityManager em;

    @Override
    public Arme add(Arme arme) {
        return armeRepository.save(arme);
    }

    @Override
    public List<Arme> addList(List<Arme> armes) {
        return armeRepository.saveAll(armes);
    }

    @Override
    public List<Arme> filter(FilterRequestDto entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Arme> cq = cb.createQuery(Arme.class);

        Root<Arme> user = cq.from(Arme.class);
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(cb.equal(user.get("active"), true));


        if(entity.getEtat() != null) {
             predicates.add(cb.equal(user.get("etat"), entity.getEtat()));
        }

        cq.where(predicates.toArray(new Predicate[0]));
        return em.createQuery(cq).getResultList();
    }
}
