package com.galactique.infraction.serviceimpl;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.Enquete;
import com.galactique.infraction.jpa.Infraction;
import com.galactique.infraction.repository.InfractionRepository;
import com.galactique.infraction.service.IInfraction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class InfractionImpl implements IInfraction {

    @Autowired
    InfractionRepository repository;

    @Autowired
    EntityManager em;

    @Override
    public Infraction add(Infraction infraction) {
        return repository.save(infraction);
    }

    @Override
    public boolean delete(Long id) {
        Infraction entity = repository.findById(id).get();
        if(entity != null) {
            entity.setActive(false);
            repository.save(entity);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public List<Infraction> filter(FilterRequestDto entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Infraction> cq = cb.createQuery(Infraction.class);

        Root<Infraction> user = cq.from(Infraction.class);
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(cb.equal(user.get("active"), true));
        
        if(entity.getId() != null) {
            // predicates.add(cb.between(user.get("dateCreation"), entity.get));
        }

        if(entity.getDebut() != null && entity.getFin() != null) {
             predicates.add(cb.between(user.get("date"), entity.getDebut(), entity.getFin()));
        }

        if(entity.getDepartement() != null) {
            predicates.add(cb.like(user.get("localite").get("departement"), "%"+entity.getDepartement()+"%"));
        }
        if(entity.getRegion() != null) {
            predicates.add(cb.like(user.get("localite").get("region"), "%"+entity.getRegion()+"%"));
        }
        if(entity.getArrondissement() != null) {
            predicates.add(cb.like(user.get("localite").get("arrondissement"), "%"+entity.getArrondissement()+"%"));
        }
        if(entity.getUnite() != null) {
            predicates.add(cb.like(user.get("localite").get("unite"), "%"+entity.getUnite()+"%"));
        }

        if(entity.getCuti() != null) {
            predicates.add(cb.equal(user.get("cuti"), entity.getCuti()));
        }


        cq.where(predicates.toArray(new Predicate[0]));
        return em.createQuery(cq).getResultList();
    }
}
