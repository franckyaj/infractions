package com.galactique.infraction.serviceimpl;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.ParamInfraction;
import com.galactique.infraction.repository.ParamInfractionRepository;
import com.galactique.infraction.service.IParamInfraction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ParamInfractionImpl implements IParamInfraction {


    @Autowired
    ParamInfractionRepository repository;

    @Autowired
    EntityManager em;

    @Override
    public List<ParamInfraction> filter(FilterRequestDto entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<ParamInfraction> cq = cb.createQuery(ParamInfraction.class);

        Root<ParamInfraction> user = cq.from(ParamInfraction.class);
        List<Predicate> predicates = new ArrayList<>();

        if(entity.getCategories().size() > 0) {
           // predicates.add(cb.in());
        }

        cq.where(predicates.toArray(new Predicate[0]));
        return em.createQuery(cq).getResultList();
    }

    @Override
    public List<ParamInfraction> saveList(List<ParamInfraction> liste) {
        return saveListInfraction(liste);
    }

    @Transactional
    public List<ParamInfraction> saveListInfraction(List<ParamInfraction> liste) {
        repository.deleteAll();
        return repository.saveAll(liste);
    }
}
