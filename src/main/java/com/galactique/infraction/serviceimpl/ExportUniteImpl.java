package com.galactique.infraction.serviceimpl;

import com.galactique.infraction.jpa.ExportUnite;
import com.galactique.infraction.jpa.ExportUniteDto;
import com.galactique.infraction.repository.ExportUniteRepository;
import com.galactique.infraction.service.IExportUnite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ExportUniteImpl implements IExportUnite {

    @Autowired
    ExportUniteRepository repository;

    @Override
    public List<ExportUnite> saveAll(ExportUniteDto dto) {
        return saveListExportUnit(dto);
    }

    @Override
    public List<ExportUnite> getAll() {
        return repository.findAll();

    }

    @Transactional
    public List<ExportUnite> saveListExportUnit(ExportUniteDto dto){
        boolean res = false;
        List<ExportUnite> liste = new ArrayList<>();
        repository.deleteAll();
        try{
            liste = repository.saveAll(dto.getUnites());
            res = true;
        }
        catch (Exception e) {
        }
        return liste;
    }

    @Override
    public List<String> getDeletation(String region) {
        return repository.getDelegation(region);
    }

    @Override
    public List<String> getUniteByDelegation(String delegation) {
        return repository.getUniteByDelegation(delegation);
    }
}
