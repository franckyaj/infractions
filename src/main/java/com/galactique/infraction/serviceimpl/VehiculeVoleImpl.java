package com.galactique.infraction.serviceimpl;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.Stupefiant;
import com.galactique.infraction.jpa.VehiculeVole;
import com.galactique.infraction.repository.VehiculeVoleRepository;
import com.galactique.infraction.service.IVehiculeVole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class VehiculeVoleImpl implements IVehiculeVole {

    @Autowired
    VehiculeVoleRepository repository;

    @Autowired
    EntityManager em;

    @Override
    public VehiculeVole add(VehiculeVole vehiculeVole) {
        return repository.save(vehiculeVole);
    }

    @Override
    public List<VehiculeVole> addList(List<VehiculeVole> vehiculeVoles) {
        return repository.saveAll(vehiculeVoles);
    }

    @Override
    public List<VehiculeVole> filter(FilterRequestDto entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<VehiculeVole> cq = cb.createQuery(VehiculeVole.class);

        Root<VehiculeVole> user = cq.from(VehiculeVole.class);
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(cb.equal(user.get("active"), true));

        if(entity.getMarque() != null) {
             predicates.add(cb.like(user.get("marque"), "%"+entity.getMarque()+"%"));
        }

        cq.where(predicates.toArray(new Predicate[0]));
        return em.createQuery(cq).getResultList();
    }

    @Override
    public boolean delete(Long id) {
        VehiculeVole entity = repository.findById(id).get();
        if(entity != null) {
            entity.setActive(false);
            repository.save(entity);
            return true;
        }
        else {
            return false;
        }
    }
}
