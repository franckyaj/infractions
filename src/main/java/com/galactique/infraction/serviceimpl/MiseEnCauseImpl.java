package com.galactique.infraction.serviceimpl;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.Infraction;
import com.galactique.infraction.jpa.Localite;
import com.galactique.infraction.jpa.MiseEnCause;
import com.galactique.infraction.repository.MiseEnCauseRepository;
import com.galactique.infraction.service.IMiseEnCause;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MiseEnCauseImpl implements IMiseEnCause {

    @Autowired
    MiseEnCauseRepository repository;

    @Autowired
    EntityManager em;

    @Override
    public MiseEnCause add(MiseEnCause entity) {
        return repository.save(entity);
    }

    @Override
    public List<MiseEnCause> addList(List<MiseEnCause> miseEnCauses) {
        return repository.saveAll(miseEnCauses);
    }

    @Override
    public List<MiseEnCause> filter(FilterRequestDto entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<MiseEnCause> cq = cb.createQuery(MiseEnCause.class);

        Root<MiseEnCause> user = cq.from(MiseEnCause.class);
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(cb.equal(user.get("active"), true));


        if(entity.getSituation() != null) {
             predicates.add(cb.equal(user.get("situation"), entity.getSituation()));
        }

        if(entity.getNomLike() != null) {
            predicates.add(cb.like(user.get("nom"), "%"+entity.getNomLike()+"%"));
        }

        /*if(entity.getNomLike() != null) {
            predicates.add(cb.like(user.get("prenom"), "%"+entity.getNomLike()+"%" ));
        }*/

        if(entity.getGav() != null) {
            predicates.add(cb.equal(user.get("gav"), entity.getGav()));
        }

        if(entity.getEtat() != null) {
            predicates.add(cb.equal(user.get("etat"), entity.getEtat()));
        }

        if(entity.getImplication() != null) {
            predicates.add(cb.equal(user.get("implication"), entity.getImplication()));
        }

        cq.where(predicates.toArray(new Predicate[0]));
        List<MiseEnCause> results = em.createQuery(cq).getResultList();
        List<MiseEnCause> miseEnCauses = results.stream().distinct().collect(Collectors.toList());
        return miseEnCauses;
    }



    @Override
    public boolean delete(Long id) {
        MiseEnCause entity = repository.findById(id).get();
        if(entity != null) {
            entity.setActive(false);
            repository.save(entity);
            return true;
        }
        else {
            return false;
        }
    }
}
