package com.galactique.infraction.serviceimpl;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.MiseEnCause;
import com.galactique.infraction.jpa.Stupefiant;
import com.galactique.infraction.repository.StupefiantRepository;
import com.galactique.infraction.service.IStupefiant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class StupefiantImpl implements IStupefiant {

    @Autowired
    StupefiantRepository repository;

    @Autowired
    EntityManager em;

    @Override
    public Stupefiant add(Stupefiant stupefiant) {
        return repository.save(stupefiant);
    }

    @Override
    public List<Stupefiant> addList(List<Stupefiant> stupefiants) {
        return repository.saveAll(stupefiants);
    }

    @Override
    public List<Stupefiant> filter(FilterRequestDto entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Stupefiant> cq = cb.createQuery(Stupefiant.class);

        Root<Stupefiant> user = cq.from(Stupefiant.class);
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(cb.equal(user.get("active"), true));

        if(entity.getEtat() != null) {
            predicates.add(cb.equal(user.get("etat"), entity.getEtat()));
        }

        if(entity.getNature() != null) {
            predicates.add(cb.like(user.get("nature"), "%"+entity.getNature()+"%" ));
        }

        cq.where(predicates.toArray(new Predicate[0]));
        return em.createQuery(cq).getResultList();
    }

    @Override
    public boolean delete(Long id) {
        Stupefiant entity = repository.findById(id).get();
        if(entity != null) {
            entity.setActive(false);
            repository.save(entity);
            return true;
        }
        else {
            return false;
        }
    }
}
