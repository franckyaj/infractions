package com.galactique.infraction.serviceimpl;

import com.galactique.infraction.dto.EnqueteUpdateReqDto;
import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.*;
import com.galactique.infraction.repository.EnqueteRepository;
import com.galactique.infraction.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EnqueteImpl implements IEnquete {

    @Autowired
    EnqueteRepository repository;

    @Autowired
    EntityManager em;

    @Autowired
    IInfraction iInfraction;

    @Autowired
    IMiseEnCause iMiseEnCause;

    @Autowired
    IVictime iVictime;

    @Autowired
    IArme iArme;

    @Autowired
    IStupefiant iStupefiant;

    @Autowired
    IVehiculeVole iVehiculeVole;

    @Override
    public Enquete add(Enquete enquete) {
        FilterRequestDto dto = new FilterRequestDto();
        Enquete res = new Enquete();
        dto.setReferenceProcedure(enquete.getReferenceProcedure());
        dto.setReferenceSaisine(enquete.getInfraction().getRreferenceSaisie());
        List<Enquete> entity = filter(dto);
        if(entity != null && entity.size() > 0 && enquete.getId() == null){
            res = null;
        }
        else {
            res = saveEnquete(enquete);
            //res = repository.save(enquete);

        }
        return res;
    }

    @Override
    public Enquete edit(EnqueteUpdateReqDto dto) {
        Enquete entity = repository.findById(dto.getId()).get();
        entity.setCadreJuridique(dto.getCadreJuridique());
        entity.setAffaire(dto.getAffaire());
        entity.setCoordonneesOpj(dto.getCoordonneesOpj());
        entity.setParquetCompetant(dto.getParquetCompetant());
        entity.setReferenceProcedure(dto.getReferenceProcedure());
        entity.setResumeFaits(dto.getResumeFaits());
        return repository.save(entity);
    }

    @Override
    public boolean delete(Long id) {
        Enquete entity = repository.findById(id).get();
        if(entity != null) {
            entity.setActive(false);
            Infraction i = entity.getInfraction();
            iInfraction.delete(i.getId());
            repository.save(entity);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public List<Enquete> filter(FilterRequestDto entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Enquete> cq = cb.createQuery(Enquete.class);

        Root<Enquete> user = cq.from(Enquete.class);

        Join<Enquete, MiseEnCause> enCauseSetJoin = user.join("miseencauses");

        List<Predicate> predicates = new ArrayList<>();

        predicates.add(cb.equal(user.get("active"), true));


        if(entity.getId() != null) {
            // predicates.add(cb.between(user.get("dateCreation"), entity.get));
        }

        if(entity.getCuti() != null) {
            predicates.add(cb.equal(user.get("cuti"), entity.getCuti()));
        }

        if(entity.getDebut() != null && entity.getFin() != null) {
             predicates.add(cb.between(user.get("dateCreation"), entity.getDebut(), entity.getFin()));
        }

        if(entity.getReferenceProcedureLike() != null) {
            predicates.add(cb.like(user.get("referenceProcedure"), "%"+entity.getReferenceProcedureLike()+"%" ));
        }

        if(entity.getReferenceProcedure() != null) {
            predicates.add(cb.equal(user.get("referenceProcedure"), entity.getReferenceProcedure() ));
        }

        if(entity.getUnite() != null) {
            predicates.add(cb.like(user.get("infraction").get("localite").get("unite"), "%"+entity.getUnite()+"%"));
        }

        if(entity.getDelegation() != null) {
            predicates.add(cb.like(user.get("infraction").get("localite").get("delegation"), "%"+entity.getDelegation()+"%"));
        }

        if(entity.getReferenceSaisine() != null) {
            predicates.add(cb.like(user.get("infraction").get("rreferenceSaisie"), "%"+entity.getReferenceSaisineLike()+"%" ));
        }

        if(entity.getArrondissement() != null) {
            predicates.add(cb.like(user.get("infraction").get("localite").get("arrondissement"), "%"+entity.getArrondissement()+"%"));
        }

        if(entity.getDepartement() != null) {
            predicates.add(cb.like(user.get("infraction").get("localite").get("departement"), "%"+entity.getDepartement()+"%"));
        }

        if(entity.getRegion() != null) {
            predicates.add(cb.like(user.get("infraction").get("localite").get("region"), "%"+entity.getRegion()+"%"));
        }

        if(entity.getNomLike() != null) {
            predicates.add(cb.like(enCauseSetJoin.get("nom"), "%"+entity.getNomLike()+"%"));
        }

        if(entity.getGav() != null) {
            predicates.add(cb.like(user.get("miseencauses").get("gav"), entity.getGav()));
        }

        cq.where(predicates.toArray(new Predicate[0]));
        return em.createQuery(cq).getResultList();
    }


    public Enquete saveEnquete(Enquete e) {
        List<MiseEnCause> mecs = new ArrayList<>();
        List<Victime> victimes = new ArrayList<>();
        List<Stupefiant> stupefiants = new ArrayList<>();
        List<Arme> armes = new ArrayList<>();
        List<VehiculeVole> vehiculeVoles = new ArrayList<>();

        if(e.getMiseencauses() != null && e.getMiseencauses().size()>0){
            for(MiseEnCause m: e.getMiseencauses()) {
                MiseEnCause i = saveMec(m);
                mecs.add(i);
                i = new MiseEnCause();
            }
            e.setMiseencauses(mecs);
        }

        if(e.getVictimes() != null && e.getVictimes().size()>0){
            for(Victime m: e.getVictimes()) {
                Victime i = saveVictime(m);
                victimes.add(i);
                i = new Victime();
            }
            e.setVictimes(victimes);
        }

        if(e.getArmes() != null && e.getArmes().size()>0){
            for(Arme m: e.getArmes()) {
                Arme i = saveArme(m);
                armes.add(i);
                i = new Arme();
            }
            e.setArmes(armes);
        }

        if(e.getStupefiants() != null && e.getStupefiants().size()>0){
            for(Stupefiant m: e.getStupefiants()) {
                Stupefiant i = saveStup(m);
                stupefiants.add(i);
                i = new Stupefiant();
            }
            e.setStupefiants(stupefiants);
        }

        if(e.getVehiculeVoles() != null && e.getVehiculeVoles().size()>0){
            for(VehiculeVole m: e.getVehiculeVoles()) {
                VehiculeVole i = saveVehicule(m);
                vehiculeVoles.add(i);
                i = new VehiculeVole();
            }
            e.setVehiculeVoles(vehiculeVoles);
        }

        return repository.save(e);
    }

    public MiseEnCause saveMec(MiseEnCause m) {
        return iMiseEnCause.add(m);
    }

    public Victime saveVictime(Victime m) {
        return iVictime.add(m);
    }

    public Arme saveArme(Arme m) {
        return iArme.add(m);
    }

    public VehiculeVole saveVehicule(VehiculeVole m) {
        return iVehiculeVole.add(m);
    }

    public Stupefiant saveStup(Stupefiant m) {
        return iStupefiant.add(m);
    }
}
