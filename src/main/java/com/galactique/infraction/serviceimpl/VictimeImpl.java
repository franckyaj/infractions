package com.galactique.infraction.serviceimpl;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.VehiculeVole;
import com.galactique.infraction.jpa.Victime;
import com.galactique.infraction.repository.VictimeRepository;
import com.galactique.infraction.service.IVictime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class VictimeImpl implements IVictime {

    @Autowired
    VictimeRepository victimeRepository;

    @Autowired
    EntityManager em;


    @Override
    public Victime add(Victime entity) {
        return victimeRepository.save(entity);
    }

    @Override
    public List<Victime> addList(List<Victime> victimes) {
        return victimeRepository.saveAll(victimes);
    }

    @Override
    public List<Victime> filter(FilterRequestDto entity) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Victime> cq = cb.createQuery(Victime.class);

        Root<Victime> user = cq.from(Victime.class);
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(cb.equal(user.get("active"), true));


        /*if(entity.getDebut() != null && entity.getFin() != null) {
            predicates.add(cb.between(user.get("dateCreation"), entity.getDebut(), entity.getFin()));
        }*/

        cq.where(predicates.toArray(new Predicate[0]));
        return em.createQuery(cq).getResultList();
    }

    @Override
    public boolean delete(Long id) {
        Victime entity = victimeRepository.findById(id).get();
        if(entity != null) {
            entity.setActive(false);
            victimeRepository.save(entity);
            return true;
        }
        else {
            return false;
        }
    }
}
