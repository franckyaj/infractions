package com.galactique.infraction.serviceimpl;

import com.galactique.infraction.dto.LocaliteDto;
import com.galactique.infraction.dto.RegionDto;
import com.galactique.infraction.dto.UniteDto;
import com.galactique.infraction.dto.UniteListDto;
import com.galactique.infraction.jpa.Arme;
import com.galactique.infraction.jpa.Infraction;
import com.galactique.infraction.jpa.Localite;
import com.galactique.infraction.jpa.Unite;
import com.galactique.infraction.repository.LocaliteRepository;
import com.galactique.infraction.repository.UnniteRepository;
import com.galactique.infraction.service.ILocalite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class LocaliteImpl implements ILocalite {

    @Autowired
    LocaliteRepository localiteRepository;

    @Autowired
    UnniteRepository unniteRepository;

    @Autowired
    EntityManager em;

    @Override
    public List<Localite> filter(Localite entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Localite> cq = cb.createQuery(Localite.class);

        Root<Localite> user = cq.from(Localite.class);
        List<Predicate> predicates = new ArrayList<>();


        if(entity.getId() != null) {
             predicates.add(cb.equal(user.get("id"), entity.getId()));
        }

        if(entity.getRegion() != null) {
            predicates.add(cb.equal(user.get("region"), entity.getRegion()));
        }

        if(entity.getDepartement() != null) {
            predicates.add(cb.equal(user.get("departement"), entity.getDepartement()));
        }

        if(entity.getArrondissement() != null) {
            predicates.add(cb.equal(user.get("arrondissement"), entity.getArrondissement()));
        }

        cq.where(predicates.toArray(new Predicate[0]));
        List<Localite> liste = em.createQuery(cq).getResultList();

        return liste;
    }

    @Override
    public List<Localite> findAll() {
        return localiteRepository.findAll();
    }

    @Override
    public List<Localite> add(LocaliteDto dto) {
        List<Localite> res = saveListLocalite(dto);
        return res;
    }

    //@Async
    @Transactional
    public List<Localite> saveListLocalite(LocaliteDto dto) {
        List<Localite> liste = new ArrayList<>();
        localiteRepository.deleteAll();
        try{
            liste = localiteRepository.saveAll(dto.getLocalites());
        }
        catch(Exception e) {}
        return liste;
    }

    @Override
    public Localite save(Localite entity) {
        return localiteRepository.save(entity);
    }

    @Override
    public boolean delete(Long id) {
        Localite entity = localiteRepository.findById(id).get();
        if(entity != null) {
           // entity.setActive(false);
            localiteRepository.save(entity);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public List<String> getDepartement(String region) {
        return localiteRepository.getDepartement(region);
    }

    @Override
    public List<String> getArrondissement(String dept) {
        return localiteRepository.getArrondissement(dept);
    }

    @Override
    public List<Unite> saveListUnite(UniteListDto uniteListDto) {
        return unniteRepository.saveAll(uniteListDto.getUnites());
    }

    @Override
    public List<Unite> filterUnite(UniteDto entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Unite> cq = cb.createQuery(Unite.class);

        Root<Unite> user = cq.from(Unite.class);
        List<Predicate> predicates = new ArrayList<>();



        if(entity.getRegion() != null) {
            predicates.add(cb.equal(user.get("region"), entity.getRegion()));
        }


        cq.where(predicates.toArray(new Predicate[0]));
        List<Unite> liste = em.createQuery(cq).getResultList();

        return liste;
    }

    @Override
    public List<String> getRegions() {
        return localiteRepository.getRegion();
    }
}
