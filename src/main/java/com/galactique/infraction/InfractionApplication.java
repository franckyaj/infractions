package com.galactique.infraction;

import jdk.javadoc.internal.doclint.Env;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableEurekaClient
public class InfractionApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfractionApplication.class, args);
	}

}
