package com.galactique.infraction.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ToolRespDto implements Serializable {

    private List<String> noms = new ArrayList<>(); //personne
    private List<String> marques = new ArrayList<>(); //vehicule
    private List<String> types = new ArrayList<>(); //arme
    private List<String> natures = new ArrayList<>(); //stupefiant
    private List<String> regions = new ArrayList<>();
    private List<String> categories = new ArrayList<>(); //infraction categorie
}
