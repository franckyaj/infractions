package com.galactique.infraction.dto;

import com.galactique.infraction.jpa.Arrondissement;
import com.galactique.infraction.jpa.Departement;
import com.galactique.infraction.jpa.Localite;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RegionDto implements Serializable {

    private List<Localite> regions = new ArrayList<>();
    private List<Departement> departements = new ArrayList<>();
    private List<Arrondissement> arrondissements = new ArrayList<>();
}
