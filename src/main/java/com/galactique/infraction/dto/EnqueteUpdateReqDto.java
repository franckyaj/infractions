package com.galactique.infraction.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EnqueteUpdateReqDto {

    private Long id;
    private String referenceProcedure;
    private String parquetCompetant;
    private String affaire;
    private String cadreJuridique;
    private String coordonneesOpj;
    private String resumeFaits;
}
