package com.galactique.infraction.dto;

import com.galactique.infraction.enumeration.TypeSexe;
import com.galactique.infraction.enumeration.TypeStatut;
import com.galactique.infraction.jpa.Enquete;
import com.galactique.infraction.jpa.MiseEnCause;
import com.galactique.infraction.jpa.Victime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StatEnqueteRespDto2 implements Serializable {

    private Long id;
    private String infraction;
    private String gav;
    private TypeSexe sexe;
    private int majeur;
    private boolean camer;
    private String situation;
    private String arrondissemeent;
    private String departement;
    private String region;
    private String delegation;
    private String unite;

    public StatEnqueteRespDto2(Enquete entity, String infraction, MiseEnCause m ) {

        this.id = entity.getId();
         if(m.getDateNaissance() != null) {
             this.majeur = this.isMaj(m.getDateNaissance(), entity.getDateCreation());
         }
         else {
            this.majeur = 3;
         }
         this.gav = m.getGav();
         this.infraction = infraction;
         this.sexe = m.getSexe();
         this.camer = m.isCameroonian();
         this.situation = m.getSituation();
         this.arrondissemeent = entity.getInfraction().getLocalite().getArrondissement();
        this.departement = entity.getInfraction().getLocalite().getDepartement();
        this.region = entity.getInfraction().getLocalite().getRegion();
        this.delegation = entity.getInfraction().getLocalite().getDelegation();
        this.unite = entity.getInfraction().getLocalite().getUnite();


    }

    private int isMaj(Date date1, Date date2) {
        LocalDate birth = date1.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate toDate = date2.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Period p = Period.between(birth, toDate);
        int age = p.getYears();
        System.out.println("age " + age);
        if(age > 18 ) {
            return 1;
        }
        else {
            return 0;
        }

    }


}
