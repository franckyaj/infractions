package com.galactique.infraction.dto;

import com.galactique.infraction.enumeration.TypeSexe;
import com.galactique.infraction.jpa.Enquete;
import com.galactique.infraction.jpa.MiseEnCause;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnqueteMecDto implements Serializable {

    private Long enqueteID;
    private Long mecId;
    private String nom;
    private String prenom;
    private String gav;
    private List<String> infractions;
    private String situation;
    private String implication;
    private TypeSexe sexe;
    private String etat;
    private Date dateInfraction;
    private String numeroCni;
    private String alias;
    private Date dateNaissance;
    private boolean cameroonian;
    private String telephone;
    private String cni;
    private String lieuNaissance;
    private String adresse;
    private boolean neevers;

    public EnqueteMecDto(Enquete e, MiseEnCause m) {
        this.enqueteID= e.getId();
        this.etat = m.getEtat();
        this.mecId = m.getId();
        this.infractions = e.getInfraction().getInfractionCommises();
        this.gav = m.getGav();
        this.neevers = m.isNeevers();
        this.implication = m .getImplication();
        this.nom = m.getNom();
        this.prenom = m.getPrenom();
        this.situation = m.getSituation();
        this.sexe = m.getSexe();
        this.setCameroonian(m.isCameroonian());
        this.dateNaissance = m.getDateNaissance();
        this.alias = m.getAlias();
        this.lieuNaissance = m.getLieuNaissance();
        this.adresse = m.getAdresse();
        this.telephone = m.getTelephone();
        this.numeroCni = m.getNumeroCni();
        this.dateInfraction = e.getInfraction().getDate();
    }
}
