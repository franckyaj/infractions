package com.galactique.infraction.dto;

import com.galactique.infraction.jpa.Unite;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UniteListDto {

    private List<Unite> unites;
}
