package com.galactique.infraction.dto;

import com.galactique.infraction.jpa.Enquete;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StatEnqueteRespDto1 implements Serializable {

    private String region;
    private String arrondissement;
    private String departement;
    private String typeSaisie;
    private Long id;
    private String delegation;
    private String unite;

    public StatEnqueteRespDto1(Enquete entity) {
        this.region= entity.getInfraction().getLocalite().getRegion();
        this.arrondissement = entity.getInfraction().getLocalite().getArrondissement();
        this.departement = entity.getInfraction().getLocalite().getDepartement();
        this.typeSaisie = entity.getInfraction().getTypeSaisie();
        this.id = entity.getId();
        this.unite = entity.getInfraction().getLocalite().getUnite();
        this.delegation = entity.getInfraction().getLocalite().getDelegation();
    }
}
