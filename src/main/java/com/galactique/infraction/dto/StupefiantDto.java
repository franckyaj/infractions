package com.galactique.infraction.dto;

import com.galactique.infraction.jpa.Stupefiant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StupefiantDto {

    private List<Stupefiant> stupefiants;
}
