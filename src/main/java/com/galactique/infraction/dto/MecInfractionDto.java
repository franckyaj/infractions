package com.galactique.infraction.dto;

import com.galactique.infraction.enumeration.TypeSexe;
import com.galactique.infraction.jpa.Enquete;
import com.galactique.infraction.jpa.Infraction;
import com.galactique.infraction.jpa.MiseEnCause;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.w3c.dom.stylesheets.LinkStyle;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MecInfractionDto implements Serializable {

    private  String nom;
    private String prenom;
    private List<String> infractionCommise;
    private String categorieInfraction;
    private String gav;
    private String etat;
    private String situation;
    private Date date;
    private String alias;
    private String implication;
    private TypeSexe sexe;
    private Date dateNaissance;
    private String telephone;
    private boolean nationalite;
    private String numCni;
    private String lieuNaissance;
    private String adresse;
    private Long mecId;
    private boolean neevers;

    public  MecInfractionDto(Enquete e, MiseEnCause m) {
        this.nom = m.getNom();
        this.prenom = m.getPrenom();
        this.infractionCommise = e.getInfraction().getInfractionCommises();
        this.categorieInfraction = e.getInfraction().getCategorie();
        this.gav = m.getGav();
        this.etat = m.getEtat();
        this.mecId = m.getId();
        this.situation = m.getSituation();
        this.date = e.getInfraction().getDate();
        this.alias = m.getAlias();
        this.lieuNaissance = m.getLieuNaissance();
        this.adresse = m.getAdresse();
        this.implication = m.getImplication();
        this.dateNaissance = m.getDateNaissance();
        this.numCni = m.getNumeroCni();
        this.telephone = m.getTelephone();
        this.nationalite = m.isCameroonian();
        this.sexe = m.getSexe();
        this.neevers = m.isNeevers();
    }
}
