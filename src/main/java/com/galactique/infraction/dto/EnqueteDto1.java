package com.galactique.infraction.dto;


import com.galactique.infraction.jpa.Infraction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnqueteDto1 {

    private String nom;
    private String prenom;
    private Infraction infraction;
    private String gav;
    private String etat;
    private String situation;
    private String telephone;
    private String numCni;
}
