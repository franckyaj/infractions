package com.galactique.infraction.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FilterRequestDto {

    private Long id;
    private Date debut;
    private Date fin;
    private String etat;
    private String situation;
    private String categorieInfraction;
    private Date dateCreation;
    private String marque;
    private String implication;
    private String gav;
    private String unite;
    private String departement;
    private String region;
    private String arrondissement;
    private String referenceProcedure;
    private String referenceProcedureLike;

    private String active;
    private List<String> categories = new ArrayList<>();
    private String nomLike;
    private String cuti;
    private String referenceSaisine;
    private String referenceSaisineLike;

    private String nature;
    private String delegation;

}
