package com.galactique.infraction.dto;

import com.galactique.infraction.jpa.ParamInfraction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ParamInfractionDto {

    private List<ParamInfraction> paramInfractions = new ArrayList<>();
}
