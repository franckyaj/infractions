package com.galactique.infraction.dto;


import com.galactique.infraction.jpa.Localite;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class LocaliteDto {

    private List<Localite> localites = new ArrayList<>();
}
