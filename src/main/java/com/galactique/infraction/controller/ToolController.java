package com.galactique.infraction.controller;

import com.galactique.infraction.dto.ToolRespDto;
import com.galactique.infraction.service.ITool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("tool")
public class ToolController {

    @Autowired
    ITool iTool;

    @GetMapping("filter")
    public ToolRespDto filter() {
        ToolRespDto dto = iTool.filter();
        return dto;
    }
}
