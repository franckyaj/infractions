package com.galactique.infraction.controller;

import com.galactique.infraction.dto.*;
import com.galactique.infraction.jpa.*;
import com.galactique.infraction.service.IExportUnite;
import com.galactique.infraction.service.ILocalite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.QueryParam;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("localite")
public class LocaliteController {

    @Autowired
    ILocalite iLocalite;

    @Autowired
    IExportUnite exportUnite;

    @PostMapping("save")
    public LocaliteDto saveLocalite(@RequestBody LocaliteDto dto) {
        LocaliteDto res = new LocaliteDto();
        List<Localite> liste = new ArrayList<>();
        liste = iLocalite.add(dto);
        res.setLocalites(liste);
        return res;
    }




    @PostMapping("departement")
    public List<String> getDept(
            @RequestBody LocaliteRqDto dto
    ) {
        return iLocalite.getDepartement(dto.getLibelle());
    }

    @PostMapping("arrondissement")
    public List<String> getArrondkissement(
            @RequestBody LocaliteRqDto dto
    ) {
        return  iLocalite.getArrondissement(dto.getLibelle());
    }

    @PostMapping("filterAndSave")
    public Localite findLocaliteAndSave(
            @RequestBody Localite dto
    ) {
        List<Localite> liste =   iLocalite.filter(dto);

        if(liste.size() > 0) {
            Localite entity = liste.get(0);
            String unite = entity.getUnite();
            if(unite == null) {
                entity.setUnite(unite);
            }
            else {
                Localite l = new Localite();
                l.setUnite(unite);
                l.setArrondissement(dto.getArrondissement());
                l.setDepartement(dto.getDepartement());
                l.setRegion(dto.getRegion());
                entity = l;
            }
            entity = iLocalite.save(entity);
            return entity;
        }
        else {
            return null;
        }

    }

    @PostMapping("unite/save")
    public ExportUniteDto saveUnite(@RequestBody ExportUniteDto dto) {
        ExportUniteDto res = new ExportUniteDto();
        List<ExportUnite> liste = new ArrayList<>();
        liste = exportUnite.saveAll(dto);
        res.setUnites(liste);
        return res;
    }

    @GetMapping("unite")
    public ExportUniteDto getUnite() {
        ExportUniteDto res = new ExportUniteDto();
        List<ExportUnite> liste = new ArrayList<>();
        liste = exportUnite.getAll();
        res.setUnites(liste);
        return res;
    }

    @GetMapping("list")
    public LocaliteDto getLocalite() {
        LocaliteDto res = new LocaliteDto();
        List<Localite> liste = new ArrayList<>();
        liste = iLocalite.findAll();
        res.setLocalites(liste);
        return res;
    }

    @GetMapping("unite/region/{region}")
    public List<String> getDelegation(@PathVariable("region") String region) {
        return exportUnite.getDeletation(region);
    }


    @PostMapping("unite/delegation")
    public List<String> getUniteByDelegation(@RequestBody DelegationDto delegation) {
        return exportUnite.getUniteByDelegation(delegation.getDelegation());
    }

    @PostMapping("unite/filter")
    public List<Unite> filterUnite(@RequestBody UniteDto dto) {
       return  iLocalite.filterUnite(dto);
    }

    @GetMapping("regions")
    public List<String> getRegions() {
        return  iLocalite.getRegions();
    }



}
