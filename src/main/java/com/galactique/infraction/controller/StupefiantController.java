package com.galactique.infraction.controller;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.dto.StupefiantDto;
import com.galactique.infraction.jpa.Stupefiant;
import com.galactique.infraction.service.IStupefiant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("stupefiant")
public class StupefiantController {

    @Autowired
    IStupefiant iStupefiant;

    @PostMapping("savelist")
    public List<Stupefiant> save(@RequestBody StupefiantDto dto) {
        return iStupefiant.addList(dto.getStupefiants());
    }

    @PostMapping("save")
    public Stupefiant save(@RequestBody Stupefiant dto) {
        return iStupefiant.add(dto);
    }

    @PostMapping("delete/{id}")
    public boolean delete(@PathVariable Long id) {
        return iStupefiant.delete(id);
    }

    @PostMapping("filter")
    public List<Stupefiant> save(@RequestBody FilterRequestDto dto) {
        return iStupefiant.filter(dto);
    }
}
