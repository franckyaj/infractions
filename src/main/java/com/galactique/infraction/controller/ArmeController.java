package com.galactique.infraction.controller;

import com.galactique.infraction.dto.ArmeDto;
import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.Arme;
import com.galactique.infraction.repository.ArmeRepository;
import com.galactique.infraction.service.IArme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("arme")
public class ArmeController {

    @Autowired
    IArme iArme;

    @Autowired
    ArmeRepository repository;

    @PostMapping("savelist")
    public List<Arme> saveList(@RequestBody ArmeDto dto) {
        return iArme.addList(dto.getArmes());
    }

    @PostMapping("save")
    public Arme saveList(@RequestBody Arme dto) {
        return iArme.add(dto);
    }

    @PostMapping("filter")
    public List<Arme> saveList(@RequestBody FilterRequestDto dto) {
        return iArme.filter(dto);
    }


}
