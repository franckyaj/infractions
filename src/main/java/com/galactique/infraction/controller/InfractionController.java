package com.galactique.infraction.controller;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.jpa.Enquete;
import com.galactique.infraction.jpa.Infraction;
import com.galactique.infraction.service.IEnquete;
import com.galactique.infraction.service.IInfraction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("infraction")
public class InfractionController {

    @Autowired
    IInfraction iInfraction;

    @Autowired
    IEnquete iEnquete;

    @PostMapping("save")
    public Infraction add(@RequestBody Infraction infraction) {
        return iInfraction.add(infraction);
    }

    @PostMapping("delete/{id}")
    public boolean delete(@PathVariable Long id) {
        return iInfraction.delete(id);
    }

    @PostMapping("filter")
    public List<Infraction> filter(@RequestBody FilterRequestDto infraction) {
        List<Infraction> results  = new ArrayList<>();
        if(infraction.getReferenceProcedure() != null) {
            List<Enquete> enquetes = iEnquete.filter(infraction);
            for(Enquete e: enquetes) results.add(e.getInfraction());
        }
        else {
            results = iInfraction.filter(infraction);
        }
        return results;
    }
}
