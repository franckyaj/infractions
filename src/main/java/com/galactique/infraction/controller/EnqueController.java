package com.galactique.infraction.controller;

import com.galactique.infraction.dto.*;
import com.galactique.infraction.jpa.Enquete;
import com.galactique.infraction.jpa.MiseEnCause;
import com.galactique.infraction.repository.EnqueteRepository;
import com.galactique.infraction.service.IEnquete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("enquete")
public class EnqueController {

    @Autowired
    IEnquete iEnquete;



    @PostMapping("save")
    public Enquete save(@RequestBody Enquete enquete) {
        return iEnquete.add(enquete);
    }

    @PostMapping("delete/{id}")
    public boolean delete(@PathVariable Long id) {
        return iEnquete.delete(id);
    }


    @PostMapping("filter")
    public List<Enquete> save(@RequestBody FilterRequestDto enquete) {
        return iEnquete.filter(enquete);
    }

    @PostMapping("filter-mec")
    public List<MecInfractionDto> filterMec(@RequestBody FilterRequestDto enquete) {
        List<MecInfractionDto> results= new ArrayList<>();
        List<Enquete> enquetes = iEnquete.filter(enquete);
        for(Enquete e: enquetes) {
            for(MiseEnCause m: e.getMiseencauses()) {
                if(m.getNom().indexOf(enquete.getNomLike()) > -1) results.add(new MecInfractionDto(e,m));
            }
        }
        return results;
    }


    @PostMapping("stats1")
    public List<StatEnqueteRespDto1> stat(@RequestBody FilterRequestDto enquete) {
        List<Enquete> enquetes = iEnquete.filter(enquete);
        List<StatEnqueteRespDto1> liste = new ArrayList<>();
        for(Enquete e: enquetes) liste.add(new StatEnqueteRespDto1(e));
        return liste;
    }


    @PostMapping("stats2")
    public List<StatEnqueteRespDto2> stat2(@RequestBody FilterRequestDto enquete) {
        List<Enquete> enquetes = iEnquete.filter(enquete);
        List<StatEnqueteRespDto2> liste = new ArrayList<>();
        for(Enquete e: enquetes) {
            for(String s: e.getInfraction().getInfractionCommises()) {
                for(MiseEnCause m: e.getMiseencauses()) {
                    liste.add(new StatEnqueteRespDto2(e, s, m));
                }
            }
        }
        return liste;
    }



    @PostMapping("edit")
    public Enquete edit(@RequestBody EnqueteUpdateReqDto enquete) {
        return iEnquete.edit(enquete);
    }
}
