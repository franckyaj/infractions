package com.galactique.infraction.controller;

import com.galactique.infraction.dto.ParamInfractionDto;
import com.galactique.infraction.jpa.ParamInfraction;
import com.galactique.infraction.repository.ParamInfractionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("param-infraction")
public class ParamInfractionController {

    @Autowired
    ParamInfractionRepository repository;

    @PostMapping("savelist")
    public ParamInfractionDto saveAll(@RequestBody ParamInfractionDto dto) {
        ParamInfractionDto res = new ParamInfractionDto();
        List<ParamInfraction> liste =  repository.saveAll(dto.getParamInfractions());
        res.setParamInfractions(liste);
        return res;
    }

    @PostMapping("add")
    public ParamInfractionDto saveAll(@RequestBody ParamInfraction dto) {
        ParamInfractionDto res = new ParamInfractionDto();
        repository.save(dto);
        List<ParamInfraction> liste = repository.findAll();
        res.setParamInfractions(liste);
        return res;
    }

    @PostMapping("delete/{id}")
    public boolean deletee(@PathParam("id") Long id) {
        boolean res = false;
        ParamInfraction entity = new ParamInfraction();
        try{
            entity = repository.findById(id).get();
            entity.setActive(false);
            repository.save(entity);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return res;
    }

    @GetMapping("list")
    public List<ParamInfraction> list() {
        //repository.deleteAll();
        return repository.findAll();
    }

    @PostMapping("infractions")
    public List<String> listInfractionComise(@RequestBody ParamInfraction dto) {
        return repository.getCategorie(dto.getDesignationInfraction());
    }
}
