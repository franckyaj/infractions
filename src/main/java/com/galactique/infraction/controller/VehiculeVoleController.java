package com.galactique.infraction.controller;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.dto.VehiculeVoleDto;
import com.galactique.infraction.jpa.VehiculeVole;
import com.galactique.infraction.service.IVehiculeVole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("vehiculevole")
public class VehiculeVoleController {

    @Autowired
    IVehiculeVole iVehiculeVole;

    @PostMapping("savelist")
    public List<VehiculeVole> saveList(@RequestBody VehiculeVoleDto dto) {
        return iVehiculeVole.addList(dto.getVehiculeVoles());
    }

    @PostMapping("delete/{id}")
    public boolean delete(@PathVariable Long id) {
        return iVehiculeVole.delete(id);
    }

    @PostMapping("save")
    public VehiculeVole save(@RequestBody VehiculeVole dto) {
        return iVehiculeVole.add(dto);
    }

    @PostMapping("filter")
    public List<VehiculeVole> filter(@RequestBody FilterRequestDto dto) {
        return iVehiculeVole.filter(dto);
    }
}
