package com.galactique.infraction.controller;

import com.galactique.infraction.dto.EnqueteMecDto;
import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.dto.MecInfractionDto;
import com.galactique.infraction.dto.MiseEnCauseDto;
import com.galactique.infraction.jpa.Enquete;
import com.galactique.infraction.jpa.MiseEnCause;
import com.galactique.infraction.repository.MiseEnCauseRepository;
import com.galactique.infraction.service.IEnquete;
import com.galactique.infraction.service.IMiseEnCause;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("miseencause")
public class MiseEnCauseController {

    @Autowired
    IMiseEnCause iMiseEnCause;

    @Autowired
    IEnquete iEnquete;

    @Autowired
    MiseEnCauseRepository repository;

    @PostMapping("savelist")
    public List<MiseEnCause> saveList(@RequestBody MiseEnCauseDto dto) {
        return iMiseEnCause.addList(dto.getMiseEnCauses());
    }

    @PostMapping("save")
    public MiseEnCause saveList(@RequestBody MiseEnCause dto) {
        return iMiseEnCause.add(dto);
    }

    @PostMapping("filter")
    public List<EnqueteMecDto> saveList(@RequestBody FilterRequestDto dto) {
        List<EnqueteMecDto> results = new ArrayList<>();
        List<Enquete> enquetes = iEnquete.filter(dto);
        for(Enquete e: enquetes) {
            for(MiseEnCause m: e.getMiseencauses()) {
                results.add(new EnqueteMecDto(e,m));
            }
        }
        List<EnqueteMecDto> liste = results;

        if(dto.getNomLike() != null && dto.getNomLike().length() > 0) {
            liste = results.stream()
                    .filter(x -> x.getNom().toUpperCase().contains(dto.getNomLike().toUpperCase()))
                    .collect(Collectors.toList());
        }

        return liste;
    }


    @PostMapping("infractions")
    public List<MecInfractionDto> getInfractions(@RequestBody FilterRequestDto dto) {
        List<MecInfractionDto> results = new ArrayList<>();
        List<MiseEnCause> miseEnCauses = iMiseEnCause.filter(dto);
        for(MiseEnCause m: miseEnCauses) {
            for(Enquete e: m.getEnquetes()) {
               for(MiseEnCause i: e.getMiseencauses())  results.add(new MecInfractionDto(e, i));
            }
        }


        return results;
    }


    @PostMapping("delete/{id}")
    public boolean delete(@PathVariable Long id) {
        return iMiseEnCause.delete(id);
    }
}
