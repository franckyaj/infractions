package com.galactique.infraction.controller;

import com.galactique.infraction.dto.FilterRequestDto;
import com.galactique.infraction.dto.VictimeDto;
import com.galactique.infraction.jpa.Victime;
import com.galactique.infraction.service.IVictime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("victime")
public class VictimeController {

    @Autowired
    IVictime iVictime;

    @PostMapping("savelist")
    public List<Victime> saveList(@RequestBody VictimeDto dto) {
        return iVictime.addList(dto.getVictimes());
    }

    @PostMapping("save")
    public Victime save(@RequestBody Victime dto) {
        return iVictime.add(dto);
    }

    @PostMapping("delete/{id}")
    public boolean delete(@PathVariable Long id) {
        return iVictime.delete(id);
    }

    @PostMapping("filter")
    public List<Victime> saveList(@RequestBody FilterRequestDto dto) {
        return iVictime.filter(dto);
    }
}
